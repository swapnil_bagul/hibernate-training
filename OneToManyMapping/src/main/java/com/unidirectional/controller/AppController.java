package com.unidirectional.controller;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unidirectional.Repository.CustomerRepository;
import com.unidirectional.Repository.OrdersRepository;
import com.unidirectional.entity.Customer;
import com.unidirectional.entity.Orders;

@RestController
@RequestMapping("OneToManyMapping")
public class AppController {

	@Autowired
	private CustomerRepository customerRepository;

	@Autowired
	private OrdersRepository ordersRepository;

	@GetMapping("/get-customer/{customerId}")
	private ResponseEntity<Customer> getCustomerInfo(@PathVariable Integer customerId) {
		Customer cust = customerRepository.getCustomerInfoBycustomerId(customerId);
		Set<Orders> orders = new HashSet<>();
		for (Orders o : cust.getOrders()) {
			orders.add(new Orders(o.getOrderId(), o.getOrderName(), o.getOrderPrice()));
		}
		cust.setOrders(orders);
		return new ResponseEntity<Customer>(cust, HttpStatus.OK);
	}

	@PostMapping("/save-customer/")
	private ResponseEntity<Customer> createCustomer(@RequestBody Customer customer) {
		Customer cust=customerRepository.save(customer);
		return new ResponseEntity<>(cust, HttpStatus.OK);
	}

	@PostMapping("/place-orders/")
	private ResponseEntity<String> placeOrder(@RequestBody HashSet<Orders> orders) {
		String message = "";
		for (Orders o : orders) {
			Optional<Customer> cust = customerRepository.findById(o.getCustomer().getCustomerId());
			if (cust.isPresent()) {
				ordersRepository.save(o);
				message = "the Orders have been placed".toUpperCase();
			} else {
				message = "Something went wrong...Order not placed".toUpperCase();
			}
		}
		return new ResponseEntity<>(message, HttpStatus.OK);
	}

}
