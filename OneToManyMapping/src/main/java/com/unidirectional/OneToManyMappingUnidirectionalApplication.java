package com.unidirectional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneToManyMappingUnidirectionalApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneToManyMappingUnidirectionalApplication.class, args);
	}

}
