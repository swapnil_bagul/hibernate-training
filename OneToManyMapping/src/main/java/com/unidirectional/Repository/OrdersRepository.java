package com.unidirectional.Repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unidirectional.entity.Orders;

public interface OrdersRepository extends JpaRepository<Orders, Integer> {

	Optional<Orders> getAllCustomersByorderName(String orderName);
}
