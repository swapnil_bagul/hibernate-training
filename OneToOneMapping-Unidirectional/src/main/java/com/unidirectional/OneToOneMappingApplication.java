package com.unidirectional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.unidirectional.dao.CompanyRepository;
import com.unidirectional.dao.EmployeeRepository;
import com.unidirectional.entity.Company;
import com.unidirectional.entity.Employee;

@SpringBootApplication
public class OneToOneMappingApplication /* implements CommandLineRunner */ {

	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private CompanyRepository companyRepository;

	public static void main(String[] args) {
		SpringApplication.run(OneToOneMappingApplication.class, args);
	}

	/*
	 * @Override public void run(String... args) throws Exception {
	 * 
	 * System.out.
	 * println("\n ++++++INSERTING NEW EMPLOYEE ALONG WITH COMPANY IN DATABASE++++++ "
	 * );
	 * 
	 * Employee emp = new Employee(12, "Swapnil Dipak Bagul"); Company company = new
	 * Company(21, "IRIS Software,Pune"); emp.setCompany(company);
	 * 
	 * companyRepository.save(company); employeeRepository.save(emp);
	 * System.out.println("EMPLOYEE has been saved successfully...!"); System.out.
	 * println("\n ++++++INSERTING NEW EMPLOYEE ALONG WITH COMPANY IN DATABASE++++++ "
	 * );
	 * 
	 * System.out.
	 * println("\n ++++++ FETCH EXISTING EMPLOYEE ALONG WITH COMPANY IN DATABASE++++++ "
	 * );
	 * 
	 * Employee employee = employeeRepository.getEmployeeByempId(123); Company comp
	 * = employee.getCompany(); System.out.println(employee.getEmpName() +
	 * " is working for " + comp.getCompanyName() + " company");
	 * 
	 * System.out.
	 * println("\n ++++++ FETCH EXISTING EMPLOYEE ALONG WITH COMPANY IN DATABASE++++++ "
	 * );
	 * 
	 * }
	 */

}
