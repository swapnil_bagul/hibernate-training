package com.unidirectional.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unidirectional.dao.CompanyRepository;
import com.unidirectional.dao.EmployeeRepository;
import com.unidirectional.entity.Company;
import com.unidirectional.entity.Employee;

@RestController
@RequestMapping("OneToOneMapping-Unidirectional")
public class AppController {

	@Autowired
	private EmployeeRepository employeeRepository;
	@Autowired
	private CompanyRepository companyRepository;

	@PostMapping("save-employee")
	private ResponseEntity<String> saveEmployee(@RequestBody Employee emp) {
		Company comp = emp.getCompany();
		companyRepository.save(comp);
		employeeRepository.save(emp);
		return new ResponseEntity<String>("The Employee has been saved".toUpperCase(), HttpStatus.OK);
	}

	@GetMapping("get-employee/{empId}")
	private ResponseEntity<Employee> getEmployeeById(@PathVariable Integer empId) {
		Employee emp = employeeRepository.getEmployeeByempId(empId);
		return new ResponseEntity<Employee>(emp, HttpStatus.OK);
	}

}
