CREATE TABLE EMPLOYEE(
emp_id integer primary key not null,
emp_name varchar(255),
company_id integer
);

CREATE TABLE COMPANY(
company_id integer primary key not null,
company_name varchar(255)
);