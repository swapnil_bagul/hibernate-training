package com.bidirectional;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OneToOneMappingBiDirectionalApplication {

	public static void main(String[] args) {
		SpringApplication.run(OneToOneMappingBiDirectionalApplication.class, args);
	}

}
