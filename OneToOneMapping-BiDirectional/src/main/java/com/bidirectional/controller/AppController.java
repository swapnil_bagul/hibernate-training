package com.bidirectional.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bidirectional.dao.AddressRepository;
import com.bidirectional.dao.CustomerRepository;
import com.bidirectional.entity.Address;
import com.bidirectional.entity.Customer;

@RestController
@RequestMapping("OneToOneMapping-bidirectional")
public class AppController {

	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private AddressRepository addressRepository;

	@PostMapping("save-customer")
	private ResponseEntity<String> saveEmployee(@RequestBody Customer customer) {
		Address address = customer.getAddress();
		addressRepository.save(address);
		customerRepository.save(customer);
		return new ResponseEntity<String>("The customer details has been saved".toUpperCase(), HttpStatus.OK);
	}

	@GetMapping("get-customer/{custId}")
	private ResponseEntity<Customer> getEmployeeById(@PathVariable Integer custId) {
		Customer cust = customerRepository.getCustomerBycustId(custId);
		Address address = new Address(cust.getAddress().getAddressId(),cust.getAddress().getLocation(),cust.getAddress().getCity());
		cust.setAddress(address);
		return new ResponseEntity<Customer>(cust, HttpStatus.OK);
	}

	@GetMapping("get-address")
	private ResponseEntity<Address> getAddressById(@RequestParam("addressId") Integer addressId) {
		Address ads = addressRepository.getAddressByaddressId(addressId);
		Customer cust = new Customer(ads.getCustomer().getCustId(),ads.getCustomer().getCustName());
		ads.setCustomer(cust);
		//System.out.println(ads.getCustomer());
		return new ResponseEntity<Address>(ads, HttpStatus.OK);
	}
}