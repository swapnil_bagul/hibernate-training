package com.bidirectional.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bidirectional.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Integer> {
	Customer getCustomerBycustId(Integer custId);
}
