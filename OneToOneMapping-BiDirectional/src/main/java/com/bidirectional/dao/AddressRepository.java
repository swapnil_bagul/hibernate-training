package com.bidirectional.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bidirectional.entity.Address;

@Repository
public interface AddressRepository extends JpaRepository<Address,Integer> {

	Address getAddressByaddressId(Integer addressId);
}
