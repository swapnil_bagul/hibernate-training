package com.example.demo.dao;

import java.util.List;

import com.example.demo.entity.Employee;

public interface EmployeeRepository {

	Employee saveNewEmployee(Employee employee);

	Employee getEmployeeById(Integer employeeId);

	List<Employee> deleteEmployeeById(Integer employeeId);

	Employee updateExistingEmployee(Employee employee);

}
