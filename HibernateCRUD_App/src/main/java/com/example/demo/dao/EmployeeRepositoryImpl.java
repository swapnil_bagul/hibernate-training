package com.example.demo.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Employee;

@Repository
public class EmployeeRepositoryImpl implements EmployeeRepository{

	@Autowired
	private SessionFactory sessionFactory;

	public Employee saveNewEmployee(Employee employee) {
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		Integer id = (Integer) session.save(employee);
		Employee emp = session.find(Employee.class, id);
		txn.commit();
		session.close();
		return emp;
	}

	public Employee getEmployeeById(Integer employeeId) {
		Session session = sessionFactory.openSession();
		Employee emp = session.find(Employee.class, employeeId);
		session.close();
		return emp;
	}

	public List<Employee> deleteEmployeeById(Integer employeeId) {
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		Employee emp = getEmployeeById(employeeId);
		session.delete(emp);
		List<Employee> empList=session.createQuery("Select e from Employee e").getResultList();
		txn.commit();
		session.close();
		return empList;
	}

	public Employee updateExistingEmployee(Employee employee) {
		Session session = sessionFactory.openSession();
		Transaction txn = session.beginTransaction();
		session.saveOrUpdate(employee);
		txn.commit();
		Employee emp = getEmployeeById(employee.getEmployeeId());
		session.close();
		return emp;
	}
}
