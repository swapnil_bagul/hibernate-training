package com.example.demo.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class HibernateConfig {

	@Value("${spring.datasource.url}")
	private String url;
	@Value("${spring.datasource.username}")
	private String username;
	@Value("${spring.datasource.password}")
	private String password;
	@Value("${spring.datasource.driver-class-name}")
	private String driverClassName;

	@Value("${spring.jpa.properties.hibernate.dialect}")
	private String dialect;
	@Value("${spring.jpa.show-sql}")
	private String show_sql;
	@Value("${spring.jpa.hibernate.ddl-auto}")
	private String ddl_auto;

	@Bean
	public DataSource getDataSource() {
		DriverManagerDataSource ds = new DriverManagerDataSource();
		ds.setUrl(url);
		ds.setUsername(username);
		ds.setPassword(password);
		ds.setDriverClassName(driverClassName);
		return ds;
	}

	@Bean
	public LocalSessionFactoryBean getSessionFactory() {
		LocalSessionFactoryBean sf = new LocalSessionFactoryBean();
		sf.setPackagesToScan("com");
		sf.setDataSource(getDataSource());
		Properties prop = new Properties();
		prop.put("hibernate.dialect", dialect);
		prop.put("hibernate.hbm2ddl.auto", ddl_auto);
		prop.put("hibernate.show_sql", show_sql);
		sf.setHibernateProperties(prop);
		return sf;
	}

	@Bean
	public HibernateTransactionManager transactionManager() {
		HibernateTransactionManager txn = new HibernateTransactionManager();
		txn.setSessionFactory(getSessionFactory().getObject());
		return txn;
	}

}
