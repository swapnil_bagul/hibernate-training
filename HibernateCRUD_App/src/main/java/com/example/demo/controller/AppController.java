package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.EmployeeRepository;
import com.example.demo.entity.Employee;

@RestController
@RequestMapping("/employee-service")
public class AppController {

	@Autowired
	private EmployeeRepository employeeRepository;

	@PostMapping("save-employee")
	public ResponseEntity<Employee> saveNewEmployee(@RequestBody Employee employee) {
		return new ResponseEntity<>(employeeRepository.saveNewEmployee(employee), HttpStatus.OK);
	}

	@GetMapping("get-employee/{employeeId}")
	public ResponseEntity<Employee> getExistingEmployee(@PathVariable("employeeId") Integer employeeId) {
		return new ResponseEntity<>(employeeRepository.getEmployeeById(employeeId), HttpStatus.OK);
	}

	@PutMapping("update-employee")
	public ResponseEntity<Employee> updateExistingEmployee(@RequestBody Employee employee) {
		return new ResponseEntity<>(employeeRepository.updateExistingEmployee(employee), HttpStatus.OK);
	}

	@DeleteMapping("delete-employee")
	public ResponseEntity<List<Employee>> updateExistingEmployee(@RequestParam Integer employeeId) {
		return new ResponseEntity<>(employeeRepository.deleteEmployeeById(employeeId), HttpStatus.OK);
	}

}
